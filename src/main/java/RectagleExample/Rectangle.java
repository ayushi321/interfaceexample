package RectagleExample;

import java.util.Scanner;

public class Rectangle implements Rectangle_Design {

    private double length , breadth;
    private double area , perimeter;

// Default Constructor
    public Rectangle() {
        this.length = 0;
        this.breadth = 0;
    }
// Method to take input of lenght and breaadth
    @Override
    public void inputparameters() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter length of the rectangle:");
        length = sc.nextDouble();
        System.out.println("Enter breadth of the rectangle:");
        breadth = sc.nextDouble();
    }

// Method to calculate perimeter
    @Override
    public void CalculatePerimeterOfRetangle() {
        perimeter = 2*(length + breadth);
        System.out.println("Perimeter of Rectangle is :" +perimeter);
    }

// Method to calculate area
    @Override
    public void CalculateAreaOfRectagle() {
        area = length * breadth;
        System.out.println("Area of the Rectangle is :" +area);

    }

   // Varargs Example
       public  void CalculateModulus(Double ...num){
           for (double number:num) {
               System.out.println(number);
               double modulus = number % 10;
               System.out.println("Modulus is :" +modulus);
           }


        }


}
